import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({ 
      registerType: 'autoUpdate',
      strategies: 'injectManifest', 
      filename: 'service-worker.js',
      injectManifest: { injectionPoint: undefined },
      includeAssets: [ 'favicon.ico', '/img/icons/apple-touch-icon.png', '/img/icons/apple-touch-icon-152x152.png' ],
      manifest: {
        id: 'todo-list-by-stephanie-peters',
        name: 'Todo-List by S. Peters',
        description: 'Shopping list and more',
        short_name: 'Todo-List',
        lang: 'en-US',
        theme_color: '#4DBA87',
        background_color: '#fff',
        display: 'standalone',
        orientation: 'any',
        scope: '/',
        start_url: '/',
        launch_handler: {
          client_mode: 'navigate-existing',
        },
        icons: [
          {
            src: './img/icons/android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: './img/icons/android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          }
        ],
      },
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
})
