const VERSION = '1.1';
const CACHE_NAME = `todo-list-cache-v${VERSION}`;
const EXCLUDE = ['.json', 'json.php'];
const WHITELIST = ['manifest.webmanifest'];

self.addEventListener('activate', event => event.waitUntil(deleteStaleCaches));
self.addEventListener('fetch', networkFirst);

async function deleteStaleCaches() {
	const cacheNames = await caches.keys();
	await Promise.all(cacheNames
		.filter(cacheName => cacheName !== CACHE_NAME)
		.map(cacheName => caches.delete(cacheName)));
}

function isExcluded(url) {
	return !!EXCLUDE.find((entry) => url.includes(entry)) && !WHITELIST.find((entry) => url.includes(entry));
}

function networkFirst(event) {
	if (isExcluded(event.request.url)) { return; }

	event.respondWith(caches.open(CACHE_NAME).then((cache) => {
		return fetch(event.request.url).then((fetchedResponse) => {
			cache.put(event.request, fetchedResponse.clone());
			return fetchedResponse;
		}).catch(() => {
			return cache.match(event.request.url);
		});
	}));
}