<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

// ini_set('display_errors',1);
// error_reporting(E_ALL);

$userUnids = file_get_contents('names.txt');
//$logFileName = "debug.log";
$userUnid = file_get_contents('name.txt');

  if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $unid = $_SERVER['QUERY_STRING'];
//  	$logFile = fopen($logFileName , 'a');

    if(strpos($userUnids, $unid) === false) {
//      fwrite($logFile, date("Y-m-d H:i:s")." - query: '".$unid."' does not match '".$userUnids."'\n");
      header("Status: 401 not authorized for ".$unid);
      http_response_code(401);
      echo "Not authorized.";
    } else {
      $json = file_get_contents('php://input');
      //$json = htmlentities ($json, ENT_NOQUOTES);
      $jsonFileName = dirname(__FILE__)."/".$unid.".json";
      $jsonFile = fopen($jsonFileName , 'w') or die('cannot open file: '.$jsonFileName);
      if (fwrite($jsonFile, $json."\n") === FALSE) {
        header("Status: 500 JSON Missing or wrong method for: ".$jsonFileName);
  			fwrite($logFile, date("Y-m-d H:i:s")." - file: ".$jsonFileName." NOT written, JSON missing or wrong method.\n");
      }  else {
        header("Status: 200 Data Written to ".$jsonFileName);
      }
      fclose($jsonFile);
    }
//  	fclose($logFile);
    $html = "
      <html>
        <body>"
          .$json.          
        "</body>
      </html>";

    echo $html;

} else {
      header("Status: 400 JSON Missing or wrong method");
      echo "Bad Request.";
  }
// }

?>
