# todo-list

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


https://medium.com/swlh/how-to-add-drag-and-drop-to-your-vuejs-project-3fc36e7b766a
