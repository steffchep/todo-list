import './style/common.css';
import './style/style-notebook.css';
import { createApp } from 'vue';
import App from './App.vue';
import { Store } from './middleware/store';

createApp(App)
	.provide('store', Store())
	.mount('#app');

