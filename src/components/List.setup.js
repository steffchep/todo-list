import { inject, nextTick, ref } from 'vue';
import { findIndexByElement, swapItemsInList, moveItemTo } from '@/middleware';

const isLargeClass = 'is-large-font';

export const setup = async () =>  {
	const store = inject('store');
	await store.loadData();

	const todo = store.todo;
	const todoFiltered = store.todoFiltered;
	const statusMessage = ref('');
	const bigFont = ref(false);

	const saveData = async () => {
		statusMessage.value = await store.saveData();
	}

	const showInfo = (info) => {
		if (statusMessage.value === info) {
			statusMessage.value = '';
		} else {
			statusMessage.value = info;
		}
	}

	let updating = false;
	const updateTodo = async ({ oldIndex, newIndex }) => {
		if (!updating) { // event seems to get triggered twice... >.<
			updating = true;
			if (oldIndex >= 0 && newIndex >= 0) {
				moveItemTo(todo.value, oldIndex, newIndex);
				await saveData();
			}
			updating = false;
		}
	};

	const setPrio = async (entry) => {
		statusMessage.value = await store.setPrio(entry);
		await saveData();
	};

	const moveItemWithArrow = async (event, direction) => {
		const todoIndex = findIndexByElement(todo.value, event.currentTarget?.parentNode);		
		
		if (todoIndex < 0) { return; }
		if (todoIndex === 0 && direction === 'up') { return; }
		if (todoIndex === todo.value.length - 1 && direction === 'down') { return; }

		const swapWithIndex = direction === 'up' ? todoIndex - 1 : todoIndex + 1;

		swapItemsInList(todo.value, todoIndex, swapWithIndex);
		
		await nextTick();
		event.currentTarget?.focus();
		await saveData();
	}

	const setEntryName = async (entry, newName) => {
		statusMessage.value = await store.setEntryName(entry, newName);
		await saveData();
	};

	const toggleDone = async (entry) => {
		statusMessage.value = await store.toggleDone(entry);
		await saveData();
	};

	const addCost = async (entry) => {
		store.addCost(entry);
		statusMessage.value = await saveData();
	};

	const toggleFontsize = () => {
		const body = document.querySelector('#app');
		if (body.classList.contains(isLargeClass)) {
			body.classList.remove(isLargeClass)
			bigFont.value = false;
		} else {
			body.classList.add(isLargeClass);
			bigFont.value = true;
		}
	}

	return {
		todo,
		todoFiltered,
		statusMessage,
		bigFont,
		moveItemWithArrow,
		saveData,
		updateTodo,
		showInfo,
		setEntryName,
		setPrio,
		toggleDone,
		addCost,
		toggleFontsize,
	}
}
