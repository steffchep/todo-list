const prios = Object.freeze({
	'1': 'ᐃ',
	'2': '▢',
	'3':  'ᐁ',
});

export const prioKeys = Object.freeze(Object.keys(prios));

export const defaultPrio = "2";

export const prioSymbol = (priority) => prios[priority] || prios[defaultPrio];
