const doneAndPrioSorter = (a,b) => {
	if (a.done) return 1;
	if (!b.done) {
		if (a.prio === b.prio) return 0;
		if (a.prio > b.prio) return 1;
		if (a.prio < b.prio) return -1;
	}
	return -1;
};

export const sortList = (array) => {
	return array.sort(doneAndPrioSorter);
};

export const findIndexByName = (array, name) => array ? array.findIndex((item) => item.name === name) : -1;

export const findIndexByElement = (list, element) => {
	if (!element) { return -1; }
	return list.findIndex((entry) => String(entry.key) === String(element.dataset.key))
};

export const filterByPrios = (filters) => (item) => item.done || (item.prio && filters.includes(item.prio));

export const swapItemsInList = (list, index1, index2) => {
	const backup = list[index2];
	list[index2] = list[index1];
	list[index1] = backup;	
	return list;
};

export const moveItemTo = (list, fromIndex, toIndex) => {
	const item = list[fromIndex];
    list.splice(fromIndex, 1);
    list.splice(toIndex, 0, item);
    return list;
};