import { getKey, removeKey, saveKey } from './keyStore';

export const passKey = 'pass';

export const login = (password) => {
	// TODO: check with backend if matches (PHP-login-backend?)
	saveKey(passKey, password);
	return true;
}

export const isLoggedIn = () => {
	return getKey(passKey, null) !== null;
}

export const logout = () => {
	removeKey(passKey);
}