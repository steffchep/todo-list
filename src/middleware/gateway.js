import { getKey } from './keyStore';
import { passKey } from './login';

const baseGetUrl = "/";
const postUrl = "/json.php?";

const postResultHandler = (result) => {
	if (result.status === 200) {
		console.log('Saved to server');
	} else {
		throw `Save failed: ${result.status} - ${result.statusText}`
	}
}

const fetchJson = (relativePath) => {
	console.log('getting data from server...');
	return fetch(baseGetUrl + relativePath)
			.then((response) => response.json())
			.then((json) => json);
};


const writeJson = async (data, queryString = '') => {
	const postOptions = {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		method: "POST",
		body: JSON.stringify(data, null, ' ')
	}
	return fetch(postUrl + queryString, postOptions)
		.then((result) => postResultHandler(result));
};

export const save = async (json) => {
	const queryString = getKey(passKey);
	console.log('Write to server...');
	await writeJson(json, queryString);
};

export const load = async () => { 
	console.log('Loading')
	const fileName = getKey(passKey);
	return await fetchJson(fileName + '.json');
}; 

