export const getTimeString = (date = new Date()) => date
				.toLocaleString(
					'en', 
					{ 
						weekday: 'short', 
						hour12: false, 
						hour: '2-digit',
						minute: '2-digit',
						second: '2-digit',
					});

