const keyPrefix = 'todo-list:';

export const saveKey = (key, value) => {
	if (localStorage) {
		try {
			localStorage.setItem(keyPrefix + key, JSON.stringify(value));
		} catch (e) {
			console.log('ERROR in saveKey - please check the integrity of your localStorage', e);
		}
	} else {
		console.log('key not saved; local storage not available');
	}
};

export const getKey = (key, defaultValue = null) => {
	if (localStorage) {
		let value;
		try {
			value = localStorage.getItem(keyPrefix + key);
			return value ? JSON.parse(value) : defaultValue;
		} catch (e) {
			if (value) {
				return value;
			} else {
				console.log('ERROR in getKey - key not set', e);
			}
			return defaultValue;
		}
	} else {
		console.log('key not found; local storage not available');
		return defaultValue;
	}
};

export const removeKey = (key) => {
	if (localStorage) {
		try {
			localStorage.removeItem(keyPrefix + key);
		} catch (e) {
			console.log('ERROR in removeKey', e);
		}
	} else {
		console.log('key not found; local storage not available');
	}
};

export const purge = () => {
	if (localStorage) {
		for (const key in localStorage) {
			if (key.startsWith(keyPrefix)) {
				localStorage.removeItem(key);
			}
		}
	}
}