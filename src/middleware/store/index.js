import { ref, computed } from 'vue';
import { 
	load, save, 
	isLoggedIn as isLoggedInFn, 
	prioKeys, 
	sortList,
	findIndexByName,
	filterByPrios,
	getTimeString 
} from '../';


const empty = {
	todo: [],
	autocomplete: [],
}

export function Store() {
	const state = ref({
		loggedIn: isLoggedInFn(),
		filters: [ ...prioKeys ],
		data: {},  // autosave, autocompleter-list, todo-list (new format)
	});
	
	// Getters
	const isLoggedIn = computed(() => state.value.loggedIn);

	const autosave = computed(() => state.value.autosave);
	const todo = computed(() => state.value.data.todo);
	const autocomplete = computed(() => state.value.data.autocomplete);
	const filters = computed(() => state.value.filters);

	const todoFiltered = computed(() => {
		return state.value.data.todo.filter(filterByPrios(state.value.filters));
	});

	// Mutations
	const setLoggedIn = (login) => state.value.loggedIn = login;
	const setData = (data) => state.value.data = data;
	const setTodo = (list) => {
		state.value.data.todo = list;
	}
	const addTodo = (entry) => {
		if (!state.value.data.todo) {
			state.value.data.todo = [];
		}
		state.value.data.todo.push(entry);
		state.value.data.todo = sortList(state.value.data.todo);
	}
	const removeDone = () => {
		const filtered = state.value.data.todo.filter((item) => !item.done);
		state.value.data.todo = filtered;
	};

	const toggleDone = (entry) => {
		const toggle = findIndexByName(state.value.data?.todo, entry.name);
		if (toggle >= 0) {
			state.value.data.todo[toggle].done = !state.value.data.todo[toggle].done;
			state.value.data.todo = sortList(state.value.data.todo);
		}
	};

	const setPrio = (entry) => {
		const index = findIndexByName(state.value.data?.todo, entry.name);
		if (index >= 0) {
			state.value.data.todo[index].prio = entry.prio;
			state.value.data.todo = sortList(state.value.data.todo);
		}
	};

	const setEntryName = (oldEntry, newName) => {
		const index = findIndexByName(state.value.data?.todo, oldEntry.name);
		if (index >= 0) {
			state.value.data.todo[index].name = newName;
			state.value.data.todo = sortList(state.value.data.todo);
		}
	};

	const setFilters = (filterArray) => {
		state.value.filters = filterArray;
	};

	const addCost = (entry) => {
		const found = findIndexByName(state.value.data?.todo, entry.name);
		if (found >= 0) {
			state.value.data.todo[found].cost = entry.cost;
		}
	};

	const setAutocomplete = (array) => {
		state.value.data.autocomplete = array;
	};

	// actions
	const loadData = async () => {
		try {
			setData({ ...empty })
			const result = await load();
			result.todo = sortList(result.todo);
			setData(result)
		} catch(err) {
			console.error(err);
		}
	}

	const saveData = async () => {
		try {
			await save(state.value.data);
			return `Saved on ${getTimeString()}`;
		} catch(err) {
			console.error(err);
			return `${getTimeString()}: ${err}`;
		}
	}

	return {
		isLoggedIn,
		todo,
		todoFiltered,
		autocomplete,
		autosave,
		filters,
		setLoggedIn,
		setFilters,
		loadData,
		saveData,
		setTodo,
		setEntryName,
		addTodo,
		setAutocomplete,
		removeDone,
		toggleDone,
		setPrio,
		addCost,
	};
}